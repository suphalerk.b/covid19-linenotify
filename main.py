#!/usr/bin/env python
# -*- coding: utf-8 -*-

from linebot import LineBotApi
from linebot.models import ImagemapSendMessage, BaseSize, Video, ImagemapArea, ExternalLink, URIImagemapAction, MessageImagemapAction, TextSendMessage
from linebot.exceptions import LineBotApiError
import sys
import os
import requests
import json

covid_data = dict()

covid_api_url = "https://covid19.th-stat.com/api/open/today"
covid_save_json = os.getcwd()+"/covid-lasted.json"
r = requests.get(covid_api_url)

covid_data['death'] = r.json()["Deaths"]
covid_data['new_death'] = r.json()["NewDeaths"]
covid_data['infect_today'] = r.json()["NewConfirmed"]
covid_data['infect_total'] = r.json()["Confirmed"]
covid_data['recovering'] = r.json()["Hospitalized"]
covid_data['recovered'] = r.json()["Recovered"]
covid_data['new_recovered'] = r.json()["NewRecovered"]
covid_data['date'] = r.json()["UpdateDate"]

print(covid_data)
with open(covid_save_json, 'r') as f:
    saved_covid = json.load(f)


if (covid_data.items() - saved_covid.items()):
    with open(covid_save_json, 'w') as file:
        json.dump(covid_data, file)

    # VfyZGh2TO88sgcDtgYgpVIVy3pdmfPNqiufj5zfdAKM # ทดสอบระบบ

    line_notify_token = [
        'JJS5BITnVQ45Zi8G4hXGZdM6hyWldCGdUVqd6TDxW06', # แม่เอ๋ไอซ์
        'ybDrQyBbVvlFp7VeRFH0chsTHX1VVSs1mUPWMEH6WFa', # RB
        'Fb7fRjXYTDRy8BBZA3duGqEcOUil2KCnRiw3mkU9lAu', # Markitting
        'TNa3x0UOrsFaVM9P1HtyRI1SUS9KyUuBMEm7H3kZ77V', # ออกหวย
    ]

    for token in line_notify_token:
        line_notify_headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + token}

        text = "\n🇹🇭สถานะการ Covid 19 \nวันที่ %s\n\n"
        text += "🤧ผู้ติดเชื้อเพิ่ม %s ราย\n"
        text += "🤒ผู้ติดเชื้อทั้งหมด %s ราย\n"
        text += "😷กำลังรักษา %s ราย\n"
        text += "😄หายแล้ว %s ราย (+%s ราย)\n"
        text += "💀เสียชีวิต %s ราย (+%s ราย)"
        text += "\n\n\n Source by: http://covid19.ddc.moph.go.th/th/api"
        text = text % (
                covid_data['date'],
                covid_data['infect_today'],
                covid_data['infect_total'],
                covid_data['recovering'],
                covid_data['recovered'],
                covid_data['new_recovered'],
                covid_data['death'],
                covid_data['new_death']
            )

        print(covid_data)
        print("---------")

        requests.post('https://notify-api.line.me/api/notify', headers=line_notify_headers, data={'message': text})
else:
    print("No update ----")
